class Hall{
	constructor(hallName){
		this.hallName = hallName;
		this.usersInHall = [];
	}

	getHallName(hallName){
		return this.hallName;
	}

	getUsersInHall(){
		return this.usersInHall;
	}
}