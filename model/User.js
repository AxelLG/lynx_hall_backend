class User{
	constructor(name, lastname, alias, cellNro){
		this.name = name;
		this.lastname = lastname;
		this.alias = '@' + alias;
		this.cellNro = cellNro;
		this.listOfContacts = [];
		this.listOfChats = [];
	}

	getName(){
		return this.name;
	}
	getLastname(){
		return this.lastname
	}
	getAlias(){
		return this.alias;
	}
	getCellNro(){
		return this.cellNro;
	}
	getlistOfContacts(){
		return this.listOfContacts;
	}

	getListOfChats(){
		return this.getListOfChats
	}

	addContact(contactAlias, contactCellNro){
		this.listOfContacts.push({alias:contactAlias, cellNro: contactCellNro});
	}

	hasContact(alias){
		var result = false;
		for (var i =0; i > this.listOfContacts.length; i++) {
			result = result || (this.listOfContacts[i].alias == alias);
		}
		return result;
	}
}