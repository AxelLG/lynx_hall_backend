const userDaoModule = require('/dao/UserDao');
const userModule = require('/User');
const hallModule = require('/Hall');

class System{
	constructor(){
		this.userDao = new userDaoModule.UserDao();
		this.halls = [];
	}

	registerUser(name, lastName, alias, cellNro){
		if(alias == undefined || cellNro == undefined){
			this.undefinedFields();
		}
		else{
			if(this.userDao.existUserWithAlias(alias)){
				this.existUserWith(alias);
			}
			else{
				if(this.userDao.existUserWithCellNro(cellNro)){
					this.existAUserWithCellNro(cellNro)
				}
				else{
					const newUser = new userModule.User(name, lastName, alias, cellNro);
					this.userDao.save(newUser);
				}
			}
		}
	}

	logInUser(alias, cellNro){
		if(alias == undefined || cellNro == undefined){
			this.undefinedFields();
		}
		else{
			if(this.userDao.existUserWithAlias(alias)){
				this.existUserWithAlias(alias)
			}
			else{
				if(! this.userDao.existUserWithCellNro(cellNro)){
					this.doesNotExistUserWithCellNro(cellNro)
				}
				else{
					const user = this.userDao.getUser(userAlias);
					return user;
				}
			}
		}
	}

	addFriend(friendAlias, userAlias){
		if(friendAlias == undefined || userAlias == undefined){
			this.undefinedFields();
		}
		else{
			if(! this.userDao.existUserWithAlias(userAlias)){
				this.doesNotExistUserWithAlias(userAlias);
			}
			else{
				const user = this.userDao.getUser(userAlias)
				if(user.hasAContactWith(friendAlias)){
					this.userHasAfriendWithAlias(userAlias, friendAlias);
				}
				else{
					const friend = this.userDao.getUser(friendAlias);
					this.userDao.updateFriendListFor(user, friend);
					user.addFriend(friend);
				}			
			}
		}
	}

	sendMessage(originAlias, destAlias, message){
		if(message == undefined || originAlias == undefined || destAlias == undefined){
			this.undefinedFields();
		}
		else{
			if(! this.userDao.existUserWithAlias(originAlias)){
				this.doesNotExistUserWithAlias(originAlias)
			}
			else{
				const sender = this.userDao.getUser(originAlias);
				if((! user.hasContact(destAlias))|| this.userDao.existUserWithAlias(destAlias)){
					this.userHasNoContactWithOrDoesNotExit(originAlias, destAlias)
				}
				else{
					const reciver = this.userDao.getUser(destAlias);
					sender.addMessageToChatWith(destAlias, message);
					reciver.addMessageToChatWith(originAlias, message);

					this.userDao.updateUser(sender);
					this.userDao.updateUser(reciver);
				}
			}
		}
	}

	createHall(hallName){
		if(hallName == undefined){
			this.undefinedFields();
		}
		else{
			if(existHallWithName(hallName)){
				this.hallWithNameAlreadyCreated(hallName);
			}
			else{
				const newHall = new hallModule.Hall(hallName);
				this.halls.push(newHall);
			}
		}
	}

	joinToHall(hallName, userAlias){
		if(hallName == undefined || userAlias == undefined){
			this.undefinedFields();
		}
		else{
			if(! this.userDao.existUserWithAlias(userAlias)){
				this.doesNotExistUserWithAlias(userAlias);
			}
			else{
				if(! this.existHallWithName(hallName)){
					this.doesNotExistAHallWithName(hallName);
				}
				else{
					let hall = this.findHallWithName(hallName);
					const user = this.userDao.getUser(userAlias)
					hall.addUser(user);
				}
			}
			
		}
	}

	undefinedFields(){
		throw "the fields must be diferent then undefined" 
	}

	existUserWithAlias(alias){
		throw "there is already a user with the alias " + alias " ."		
	}

	doesNotExistUserWithAlias(alias){
		throw "there is no user with the alias" + alias + " ."
	}

	existAUserWithCellNro(cellNro){
		throw "there is already a user with the phone nro " + cellNro " ."		
	}

	doesNotExistUserWithCellNro(cellNro){
		throw "there is no user with the phone nro " + cellNro " ."
	}

	userHasAfriendWithAlias(userAlias, friendAlias){
		throw "the user with the alias " + userAlias " has already a friend with the alias "+ friendAlias +" ."
	}

	userHasNoContactWithOrDoesNotExit(emisor, receptor){
		throw "the user "+emisor+" has no friend with the alias "+receptor+" or the friend does not exist."
	}

	hallWithNameAlreadyCreated(hallName){
		throw "there is already a hall with the name " + hallName + " ." 
	}

	doesNotExistAHallWithName(hallName){
		throw "there is no hall with the name " + hallName + " ."
	}
}